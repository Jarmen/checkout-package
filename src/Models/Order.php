<?php

namespace lenal\checkout\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'order_number', 'fio', 'delivery_type', 'payment_type',
        'phone', 'email', 'products_data', 'status'
    ];
}
