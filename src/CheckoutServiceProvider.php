<?php

namespace lenal\checkout;

use Illuminate\Support\ServiceProvider;

class CheckoutServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind('orders', 'lenal\checkout\Helpers\Orders');
    }

    public function boot()
    {
        $this->loadViewsFrom(realpath(dirname(__FILE__)) . '/resources/views', 'checkout');
        $this->loadRoutesFrom(realpath(dirname(__FILE__)) . '/routes.php');
        $this->loadMigrationsFrom(realpath(dirname(__FILE__)) . '/migrations');
        $this->loadTranslationsFrom(realpath(dirname(__FILE__)) . '/resources/lang', 'checkout');

        $this->publishes([
            realpath(dirname(__FILE__) . '/config/checkout.php') => config_path('checkout.php')
        ], 'config');

        $this->publishes([
            base_path('vendor/lenal/checkout/') => base_path('packages/lenal/checkout')
        ]);
    }
}