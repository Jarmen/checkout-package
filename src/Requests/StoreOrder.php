<?php

namespace lenal\checkout\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // TODO add rules here
//        return [
//            'firstname' => 'required|min:2|alpha'
//        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        // TODO add your specific validation error messages here
//        return [
//            'firstname.min' => trans('checkout::checkout.order_form.firstname.min'),
//            'firstname.alpha'  => trans('checkout::checkout.order_form.firstname.alpha')
//        ];
    }
}
