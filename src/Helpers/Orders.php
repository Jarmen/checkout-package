<?php

namespace lenal\checkout\Helpers;

use lenal\checkout\Models\Order;
use lenal\cart\Facades\CartWrapper;

class Orders {

    protected $deliveries;
    protected $payments;

    public function __construct()
    {
        $this->deliveries = [
            'self' => trans('checkout::checkout.deliveries.self'),
            'address' => trans('checkout::checkout.deliveries.address'),
            'nova_poshta' => trans('checkout::checkout.deliveries.nova_poshta'),
        ];

        $this->payments = [
            'cash' => trans('checkout::checkout.payments.cash'),
            'cashless-obj' => trans('checkout::checkout.payments.cashless'),
        ];
    }

    public function createNewOrder($order_data)
    {
        $created = Order::create([
            // TODO add your data for new Order
//            'order_number' => $this->generateRandomOrderNumber(),
//            'fio' => $order_data['firstname'] . ' ' . (empty($order_data['lastname']) ? '' : $order_data['lastname']),
//            'delivery_type' => $this->deliveries[$order_data['shipping_method']],
//            'payment_type' => $this->payments[$order_data['payment_method']],
//            'phone' => $order_data['phone'],
//            'email' => $order_data['email'],
//            'products_data' => $this->getProductsData()
        ]);

        return $created;
    }

    protected function getProductsData()
    {
        $products = '';

        $cart = CartWrapper::getCartContent();

        foreach ($cart as $item) {
            $products .= $item->id . ':' . $item->qty . ':' . $item->price . ',';
        }

        return substr($products, 0, -1);
    }

    protected function generateRandomOrderNumber()
    {
        $random_number = rand(100000, 999999999);

        while ($order = Order::where('order_number', '=', $random_number)->first()) {
            $random_number = rand(100000, 999999999);
        }

        return $random_number;
    }

}
