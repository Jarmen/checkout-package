<?php

return [
    'order_form' => [
        'firstname' => [
            'min' => 'В поле Имя должно быть не менее :min символов',
            'alpha' => 'В поле Имя должны быть только буквы'
        ],
    ],

    'deliveries' => [
        'self' => 'Самовывоз',
        'address' => 'Доставка по адресу',
        'nova_poshta' => 'Новая Почта'
    ],

    'payments' => [
        'cash' => 'Наличными',
        'cashless' => 'Безналичный расчет'
    ]
];
