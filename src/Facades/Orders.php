<?php

namespace lenal\checkout\Facades;

use Illuminate\Support\Facades\Facade;

class Orders extends Facade {

    public static function getFacadeAccessor()
    {
        return 'orders';
    }
}