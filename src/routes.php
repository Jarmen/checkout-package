<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/checkout', '\lenal\checkout\Controllers\CheckoutController@index');

    Route::post('/thanks_for', '\lenal\checkout\Controllers\CheckoutController@store');
});