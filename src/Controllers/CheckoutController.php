<?php

namespace lenal\checkout\Controllers;

use App\Http\Controllers\Controller;
use lenal\cart\Facades\CartWrapper;
use lenal\checkout\Facades\Orders;
use lenal\checkout\Requests\StoreOrder;

class CheckoutController extends Controller
{
    public function index()
    {
        if (CartWrapper::getCartCount() == 0) {
            abort(404);
        }

        $cart_total = CartWrapper::getTotal();

        return view('checkout::checkout', compact('cart_total'));
    }

    public function store(StoreOrder $request)
    {
        if (CartWrapper::emptyCart()) {
            abort(404);
        }

        $order_created = Orders::createNewOrder($request->all());

        $order_number = $order_created->order_number;

        // TODO send message with order details
        //$order_sent = Orders::sendOrder($order_created);
//        mail(
//            $request->get('email'),
//            'Новый заказ',
//            'Ваш заказ GD' . $order_number,
//            'From: webmaster@example.com' . "\r\n" .
//            'Reply-To: webmaster@example.com' . "\r\n" .
//            'X-Mailer: PHP/' . phpversion()
//        );

        CartWrapper::destroyCart();

        return view('checkout::thanks_for', compact('order_number'));
    }
}
