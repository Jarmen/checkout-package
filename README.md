## lenal/checkout   
  
  Checkout package without payment or delivery integration  
  
### Install  
  
  * Add to composer.json  
  
  ```  
  "repositories": [  
          {  
              "type": "git",  
              "url":  "git@gitlab.com:Jarmen/checkout-package.git"  
          }  
      ]  
  ```   
  
  If prompted, insert login/password or create auth.json in your global composer directory
    
  * Run command  
  
  ```
  composer require lenal/checkout  
  ```  
  
  ### Basic usage  
  * Run command  
  
  ```
  php artisan vendor:publish --provider=lenal\checkout\CheckoutServiceProvider --tag=config
  ```  
  
  to copy configuration file into project
      
  * Run command  
  
  ```
    php artisan migrate
   ```  
    
  ### Package customization  
  
  * Run command  
  for laravel <= 5.4 
  
  ```
    php artisan vendor:publish --provider=lenal\checkout\CheckoutServiceProvider
  ```  
  
   for laravel >= 5.5 
    
    ```
      php artisan vendor:publish 
    ```  
   
   select package from list  
   
   All files wiil be copied to packages/lenal/checkout   
  
  * Register package namespace in composer.json  
  
  ```
    #!json
    "autoload": {
            "psr-4": {
                "lenal\\checkout\\": "packages/lenal/checkout/src"
            }
        },
   ```   
   
   * Add service provider to 'providers' section in  config/app.php  
   
   ```  
   /*  
    * Package Service Providers...  
    */  
    lenal\checkout\CheckoutServiceProvider::class,  
   ```  
   
   * Run command  
   
   ```
   composer dumpautoload
   ```
   
   To prevent namespace collision remove package from vendor
   
   ```
   composer remove lenal/checkout
   ```  
   
   
   ### Use cases and mockups  
     
   https://gitlab.com/Jarmen/checkout-package/wikis/home
  

