### Future releases

### Release v0.0.2-alpha date: 30.10.2018  
Deleted:  
 * unnecessary files  
 * non existant classes container registration from service provider  
   
Updated:  
 * orders table migration 
 * fillable fields in Order model  
 * routes  
  
Added:  
 * checkout form  
 * thank-you page  
 * 6-9 random digits order id generator   
 * lenal/cart dependencies 
 * language file for russian  
 * request object StoreOrder with examples  
  
### Release v0.0.1-alpha date: 19.10.2018  
  
Added:
 * basic package structure according to https://bitbucket.org/lenalltd/lenal_standards/wiki/automation_php
 * README.md according to "README.md must have"
 * CHANGELOG.md according to "CHANGELOG.md must have"
 * CONTRIBUTING.md according to "CONTRIBUTING.md must have"